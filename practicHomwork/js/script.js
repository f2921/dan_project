const swiper1 = new Swiper('#team_member_swiper1', {
      slidesPerView: 1,
      spaceBetween: 4,
      direction: 'horizontal',
      loop: true,

      // If we need pagination
      pagination: {
            el: '.swiper-pagination',
            clickable: true
      },

      // Navigation arrows
      navigation: {
            nextEl: '.member_next1',
            prevEl: '.member_prev1',
      },
      breakpoints: {
            '320': {
                  slidesPerView: 1,

            },
            '375': {
                  slidesPerView: 1,

            },
            '376': {
                  slidesPerView: 2,

            },
            '400': {
                  slidesPerView: 2,

            },
            '500': {
                  slidesPerView: 2,

            },
            '540': {
                  slidesPerView: 2,

            },
            '541': {
                  slidesPerView: 3,

            },
            '600': {
                  slidesPerView: 3,

            },
            '700': {
                  slidesPerView: 3,

            },
            '800': {
                  slidesPerView: 3,

            },
            '900': {
                  slidesPerView: 3,

            },
            '1000': {
                  slidesPerView: 3,

            },
            '1100': {
                  slidesPerView: 3,

            },
            '1150': {
                  slidesPerView: 3,

            },
            '1151': {
                  slidesPerView: 4,

            },
            '1200': {
                  slidesPerView: 4,

            },
            '1300': {
                  slidesPerView: 4,

            },
            '1400': {
                  slidesPerView: 4,

            },
            '1500': {
                  slidesPerView: 4,

            },
            '1600': {
                  slidesPerView: 4,

            },
            '1700': {
                  slidesPerView: 4,

            },
            '1800': {
                  slidesPerView: 4,

            },
            '1920': {
                  slidesPerView: 4,

            },
      },
      on: {
          slideChange: function () {
             let imgActive  = document.querySelector('.swiper-slide-active img').getAttribute('src')
           let changeImg  = document.querySelector('.team_member_image img').setAttribute('src',imgActive)
          },
      },
});

function renderElement(){

      let tabsEl = document.querySelectorAll('.works_tabs_wrapper');
      tabsEl.forEach((el) => {
            el.addEventListener('click',() => {
                  let tabsUl  = document.querySelector('.works_first_wrapper > .active');
                  let elData  = el.getAttribute('data-tab_id');
                  tabsUl.classList.remove('active');
                  el.classList.add('active');

                  if (elData != '0'){
                        let imgData = document.querySelectorAll('.work_flex_items');
                        imgData.forEach((el) => {
                              el.style.display = 'none'
                        })
                        let imgActive  = document.querySelectorAll('.category_'+elData+'');
                        imgActive.forEach((el) => {
                              el.style.display = 'flex'
                        })

                        let loadBtn  = document.querySelector('.show_more_imgs');
                        loadBtn.style.display = 'none';

                  }else{
                        let imgData = document.querySelectorAll('.work_flex_items');
                        imgData.forEach((el) => {
                              el.style.display = 'flex'
                        })

                        let loadBtn  = document.querySelector('.show_more_imgs');
                        loadBtn.style.display = 'block';
                  }
            });
      })

}

renderElement()

document.querySelector('.show_more_imgs').addEventListener('click',() => {
      let imgsHide  = document.querySelector('.hide_work_flex');
      let loadBtn  = document.querySelector('.show_more_imgs');
      imgsHide.style.display = 'flex';
      loadBtn.style.display = 'none';
});